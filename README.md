[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

Re version 1.0
==============

The README is used to introduce the module and provide instructions on
how to install the module, any machine dependencies it may have (for
example C compilers and installed libraries) and any other information
that should be provided before the module is installed.

A README file is required for CPAN modules since CPAN extracts the
README file from a module distribution so that people browsing the
archive can use it get an idea of the modules uses. It is usually a
good idea to provide version information here so that people can
decide whether fixes for the module are worth downloading.

INSTALLATION

Install CoreSubs from the nimiux repo: https://codeberg.org/nimiux/coresubs.git
Run: cpan Algorithm::Permute File::Type Getopt::Long::Descriptive LWP::UserAgent List::AllUtils Net::SSH2
Run this project install.sh script

DEPENDENCIES

This module requires these other modules and libraries:

File::Type
Getopt::Long::Descriptive
LWP::UserAgent
List::AllUtils
Net::SSH2

To recreate CPAN modules:

cpan All::MigrateModules
mv perl5 perl5_old
perl-migrate-modules --from perl5_old /usr/bin/perl

USAGE

SEE ALSO

NOTES
