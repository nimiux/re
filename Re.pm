package Re;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

require Exporter;

our @ISA = qw(Exporter);
our @EXPORT_OK = qw(
    $VERSION
    $associations
    convert
    repos
    runcommand
    fillpdf
);
#    fullupdate

our $VERSION = '1.0';

use File::Basename;

use CoreSubs::CoreSubs qw(
    $NULL
    boldmsg
    debugmsg
    isxterminal
    komsg
    mydie
    notify
    okmsg
    whoami
);

use CoreSubs::Proc qw(
    current_cpu_usage
    renderpdf
    run_as_root
);

use CoreSubs::Storage qw(
    ismounted
    filepath
    loopdeviceclose
    loopdeviceopen
    luks_close
    luks_open
    lvup
    maketempdir
    mount
    show_user_mounts
    sync
    umount
    vgdown
);
use Data::Dumper;
use Date::Calc qw (
    Today
    Month_to_Text
);
#use Devel::REPL;
#use File::Basename;
#use File::LibMagic;
use Algorithm::Permute;
use Devel::REPL;
use File::Type;
use LWP::UserAgent;
use Math::Base::Convert;
use Net::SSH2 qw( LIBSSH2_HOSTKEY_POLICY_ADVISORY );
use MIME::Parser;
#use Term::ANSIColor qw(:constants);
use Text::Template;
#use Carp;
#use Digest::SHA qw(sha1 sha1_hex);

# Read environment variables. TODO: Review
our $HOME = $ENV{"HOME"};
our $EXTREPOSPATH = $ENV{"EXTREPOSPATH"};
our $MYREPOSPATH = $ENV{"MYREPOSPATH"};
our $NUT_VG_NAME = $ENV{"NUTVG"};
our $NUT_LV_NAME = $ENV{"NUTLV"};
our $NUT_MOUNTPOINT = $ENV{"NUTMOUNT"};
our $MYRC_DIR = $ENV{"MYRCDIR"};

# Some objects needed
my $ft = File::Type->new;
my $ua = LWP::UserAgent->new;
my $magic = File::LibMagic->new;
my $ssh2 = Net::SSH2->new;

# Some global vars
my $OK="✓";
my $KO="✗";

sub void {
    1;
}

sub convert {
    my ($infile, $informat, $outfile, $outformat) = @_;
    debugmsg("Looking for command: infile=$infile, informat=$informat, outfile=$outfile outformat=$outformat");
    my $command = lookforcommand('echo Conversion from {$from} to {$to} not implemented', $informat, $outformat);
    #my %vars = (
    #   'in' => qq("$infile"),
    #   'from' => qq("$informat"),
    #   'out' => qq("$outfile"),
    #   'to' => qq("$outformat"),
    #);
    my %vars = (
       'in'   => $infile,
       'from' => $informat,
       'out'  => $outfile,
       'to'   => $outformat,
   );
   debugmsg(Dumper($command));
   debugmsg(Dumper(%vars));
   runcommand($command, \@_, \%vars);
}

sub lookforcommand {
    my $associations = {
        'ac3' => {
            'ac3' => 'ffmpeg -i "{$in}" -acodec ac3 "{$out}"',
            'mp3' => 'ffmpeg -i "{$in}" -acodec libmp3lame "{$out}"',
            'ogg' => 'ffmpeg -i "{$in}" -acodec libvorbis "{$out}"',
            'wav' => 'ffmpeg -i "{$in}" {$out}',
            },
        'ac3' => {
            'aac' => 'ffmpeg -i "{$in}" -acodec libfaac "{$out}"',
            'mp3' => 'ffmpeg -i "{$in}" -acodec libmp3lame "{$out}"',
            'ogg' => 'ffmpeg -i "{$in}" -acodec libvorbis "{$out}"',
            'wav' => 'ffmpeg -i "{$in}" "{$out}"',
            },
        'ape' => {
            'wav' => 'mac "{$in}" "{$out}" -d',
            },
        'avi' => {
            'divx' => 'ffmpeg -i "{$in}" -s 320x240 -vcodec msmpeg4v2 "{$out}"',
            'dv' => 'ffmpeg -i "{$in}" -s pal -r pal -aspect 4:3 -ar 48000 -ac 2 "{$out}"', #ffmpeg -i video_origine.avi -target pal-dv video_finale.dv
            'flv' => 'ffmpeg -i "{$in}" 56 -ar 44100 -b 200 -r 15 -s 320x240 -f flv "{$out}"', # mencoder $1 -o $1.flv -ovc lavc -lavcopts vcodec=flv:acodec=mp3:abitrate=56 -srate 22050
            'gif' => 'ffmpeg -i "{$in}" "{$out}',
            'img' => 'ffmpeg -i "{$in}" "{$out}%d.png"',
            'mp3' => 'ffmpeg -i "{$in}" -vn -ar 44100 -ac 2 -ab 192k -f mp3 "{$out}"',
            'mp4' => 'mencoder "{$in}" -o "{$out}" -oac copy -ovc lavc -lavcopts vcodec=mpeg1video -of mpeg', # ffmpeg -i $1 -vcodec mpeg4 -s 320x240 -b 300k -r 23.98 -acodec mp3 -ar 48000 -ab 112k -o $1.mp4 # ffmpeg -i $1 -vcodec mpeg4 -acodec mp2 $1.mp4
            'mpg' => 'ffmpeg -i "{$in}" "{$out}"', #ffmpeg -i source_video.avi -target pal-dvd -ps 2000000000 -aspect 16:9 finale_video.mpeg
            'svcd' => 'ffmpeg -i "{$in}" -target pal-svcd "{$out}"', #NTSC format: ffmpeg -i video_origine.avi -target ntsc-svcd video_finale.mpg
            'vcd' => 'ffmpeg -i "{$in}" -target pal-vcd "{$out}"', #NTSC format: ffmpeg -i video_origine.avi -target ntsc-vcd video_finale.mpg
            'wmv' => 'mencoder "{$in}" -o "{$out}" -of lavf -ovc lavc -lavcopts vcodec=wmv2 -oac lavc',
            },
        'csv' => {
            'hash' => \&csv2hash,
            },
        'doc' => {
            'epub' => 'libreoffice --convert-to epub "{$in}"',
            'html' => 'libreoffice --convert-to html:HTML:EmbedImages "{$in}"',
            'pdf' => 'libreoffice --convert-to pdf "{$in}"',
            'txt' => 'libreoffice --convert-to "txt:Text (encoded):UTF8" "{$in}"',
            },
        'epub' => {
            'pdf' => 'ebook-convert {$in} {$out}',
            },
        'file' => {
            'array' => \&file2array,
            },
        'flv' => {
            'img' => 'ffmpeg -i {$in} {$out}%d.png',
            'ogg' => 'ffmpeg -i {$in} -vcodec libtheora -sameq -acodec libvorbis -ac 2 -sameq {$out}', # ffmpeg -i ${INPUTFILE} -vn -ar 44100 -ac 2 -ab 160k -f ogg ${OUTPUTFILE}
            },
        'gif' => {
            'jpg' => 'convert {$in} {$out}',
            'pdf' => 'convert {$in} {$out}',
            'png' => 'convert {$in} {$out}',
            },
        'img' => {
            'mpg' => 'ffmpeg -f image2 -i {$in}%d.png {$out}',
            },
        'jpg' => {
            'gif' => 'convert {$in} {$out}',
            'pdf' => 'convert {$in} {$out}',
            'png' => 'convert {$in} {$out}',
            },
        'mkv' => {
            'mp3' => 'ffmpeg -i {$in} {$out}',
            'mp4' => 'ffmpeg -i {$in} {$out}',
            },
        'mp3' => {
            'ogg' => 'ffmpeg -i {$in} -c:a libvorbis -q:a 4 {$out}', # mplayer -ao pcm:waveheader -ao pcm:file="${tmpfile}" "${infile}" ; oggenc -o "${filename}.ogg" "${tmpfile}"
            'wav' => 'ffmpeg -i {$in} {$out}',
            },
        'mp4' => {
            'img' => 'ffmpeg -i {$in} {$out}%d.png',
            'mp3' => 'ffmpeg -i "{$in}" "{$out}"',
            'mp4' => 'ffmpeg -i {$in} -vcodec mpeg4 -s xga -acodec mp2 {$out}',
            },
        'mpg' => {
            'avi' => 'ffmpeg -i {$in} {$out}',
            'flv' => 'mencoder {$in} -o {$out} -ovc lavc -lavcopts vcodec=flv:acodec=mp3:abitrate=56 -srate 22050',
            'img' => 'ffmpeg -i {$in} {$out}%d.png',
            'wmv' => 'mencoder {$in} -o {$out} -of lavf -ovc lavc -lavcopts vcodec=wmv2 -oac lavc',
            },
        'mts' => {
            'avi' => 'mencoder {$in} -o {$out} -oac copy -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=10000 -fps 60000/1001 -vf scale=1024:768', # mencoder $1 -o $1.avi -oac copy -ovc lavc -lavcopts vcodec=mpeg -vf scale=800:500 # ffmpeg -i $1 -f avi -vcodec dvvideo -s pal -aspect 16:9 -qscale 4 -acodec pcm_s16le -ac 2 $1.avi
            },
        'odt' => {
            'epub' => 'libreoffice --convert-to epub {$in}',
            'html' => 'libreoffice --convert-to html:HTML:EmbedImages {$in}',
            'pdf' => 'libreoffice --convert-to pdf {$in}',
            'txt' => 'libreoffice --convert-to "txt:Text (encoded):UTF8" {$in}',
            },
        'ogg' => {
            'aac' => 'ffmpeg -i {$in} -acodec libfaac {$out}',
            'ac3' => 'ffmpeg -i {$in} -acodec ac3 {$out}',
            'mp3' => 'ffmpeg -i "{$in}" -acodec libmp3lame "{$out}"', # ogg123 -d wav -f - file.ogg | lame - file.mp3
            'mpg' => 'ffmpeg -i {$in} -s 720x576 -vcodec mpeg2video -acodec mp3 {$out}',
            'wav' => 'ffmpeg -i {$in} {$out}',
            },
        'pdf' => {
            'gaf' => 'ffmmpeg...',
            'jpg' => 'convert {$in} {$out}',
            'pdf' => \&void,
            'png' => 'convert {$in} {$out}',
            'txt' => 'pdftotext {$in} {$out}',
            },
        'png' => {
            'gif' => 'convert {$in} {$out}',
            'jpg' => 'convert {$in} {$out}',
            'pdf' => 'convert {$in} {$out}',
            },
        'qcow2' => {
            'raw' => 'qemu-img convert -f qcow2 {$in} -O raw {$out}',
            },
        'raw' => {
            'vdi' => 'VBoxManage convertfromraw --format VDI {$in} {$out}',
            },
        'txt' => {
            'pdf' => 'libreoffice --convert-to pdf {$in}',
            'html' => 'pandoc {$in} -o {$out}',
            },
        'wav' => {
            'aac' => 'ffmpeg -i {$in} -acodec libfaac {$out}',
            'ac3' => 'ffmpeg -i {$in} -acodec ac3 {$out}',
            'mp3' => 'ffmpeg -i {$in} -acodec libmp3lame {$out}', #ffmpeg -i son_origine.avi -vn -ar 44100 -ac 2 -ab 192k -f mp3 son_final.mp3
            'ogg' => 'ffmpeg -i {$in} -acodec libmp3vorbis {$out}',
            },
        'webp' => {
            'png' => 'convert {$in} {$out}',
            'jpg' => 'convert {$in} {$out}',
            },
    };

    my ($default, $firstkey, $secondkey) = @_;
    $secondkey = $secondkey || "";
    debugmsg("Looking for command: $firstkey | $secondkey | $default");
    my $command = exists($associations->{$firstkey}) ? $associations->{$firstkey} : $default;
    debugmsg("Command is $command");
    if ( ref($command) eq "HASH" ) {
        $command = exists($command->{$secondkey}) ? $command->{$secondkey} : $default;
    }
    return $command,
}

sub runcommand {
    my ($command, $args, $vars) = @_;
    my $commandtype = ref($command);
    debugmsg("Command type is: $commandtype");
    debugmsg(Dumper($args));
    debugmsg(Dumper($vars));
    if ( $commandtype eq "CODE" ) {
        return $command->(@$args);
    } else {
        my $template = Text::Template->new(TYPE => 'STRING',
            SOURCE => $command);
        my $rendered = $template->fill_in(HASH => $vars);
        debugmsg("Running command: $rendered");
        return system $rendered;
    }
}

sub list {
     my ($search) = @_;
     my @list = sort grep { Re->can($_) } keys %Re::;
     @list = grep { /$search/ } @list if $search;
     @list = map { $_ =~ tr/_/-/ ; $_ } @list;
     print join("\n", @list);
}

sub perlsh {
    debugmsg("Perl SH");
    #system("perl", "-del");
    system("rlwrap", "perl" , "-e",
           'do {
                print("perl> ");
                $_x = <>;
                chomp $_x;
                print(eval($_x) . "\n");
            } while ($_x ne "q")');
}

sub perlrepl {
    debugmsg("Perl REPL");
    my $repl = Devel::REPL->new;
    $repl->load_plugin($_) for qw(History LexEnv);
    $repl->run;
}

sub print_unicode {
    print chr for 0xf000 .. 0xf2ff;
}

sub packhexa {
    while (<STDIN>) {
        chomp;
        print pack "H*", $_;
    }
}

sub fillpdf {
    my ($inpdf, $outpdf, $fields) = @_;
    my ($year, $month, $day) = Today();
    $fields->{ano} = substr($year, -2);
    $fields->{mes} = Month_to_Text($month, 4);
    $fields->{dia} = $day;
    renderpdf($inpdf, $outpdf, $fields);
}

sub reducepdf {
    # Summary of -dPDFSETTINGS:

    # -dPDFSETTINGS=/screen lower quality, smaller size. (72 dpi)
    #-dPDFSETTINGS=/ebook for better quality, but slightly larger pdfs. (150 dpi)
    #-dPDFSETTINGS=/prepress output similar to Acrobat Distiller "Prepress Optimized" setting (300 dpi)
    #-dPDFSETTINGS=/printer selects output similar to the Acrobat Distiller "Print Optimized" setting (300 dpi)
    #-dPDFSETTINGS=/default selects output intended to be useful across a wide variety of uses, possibly at the expense of a larger output file

    system "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf input.pdf"
}

sub cropvideo {
    my ($infile, $outfile, $outw, $outh, $x, $y) = @_;
    system "ffmpeg -i $infile -filter:v \"crop=$outw:$outh:$x:$y\" $outfile";
}

sub clip_media {
    # Extracts video from $infile to $outfile starting at position $start and ending at position
    # $endoorduration. If first character in $endorduration is % then extracts to $endorduration
    # Other valid command for video is:
    #  mencoder -ss 00:30:00 -endpos 00:00:05 -oac copy -ovc copy originalfile -o newfile
    #
    # $start HH:MM:SS.ss
    # $end HH:MM:SS.ss
    # $duration %HH:MM:SS.ss

    my ($infile, $outfile, $start, $endorduration) = @_;
    if ($endorduration =~ /%/) {
        $endorduration = substr $endorduration, 1;
        $endorduration = "-t $endorduration";
    } else {
        $endorduration = "-to $endorduration";
    }
    system "ffmpeg -ss $start $endorduration -i $infile $outfile";
}

sub window2img {
    debugmsg "In window ";
    debugmsg(Dumper(@_));
    my ($format, $outfile, $window) = @_;
    my $wid;
    unless ( $window ) {
        $wid = "-root";
    } else {
        okmsg("Click window to capture...");
        $wid = `xwininfo | grep "Window id" | cut -d" " -f 4`;
        $wid = "-id $wid";
        chomp $wid;
    }
    system "xwd $wid | xwdtopnm $NULL | pnmto$format 1> $outfile 2>&1";
}

sub img2video {
    # $imagestring has the format name-%Nd.png where N represents the image number
    my ($imagestring, $output) = @_;
    my %vars = (
        'image' => qq("$imagestring"),
        'out'   => qq("$output"),
    );
    return runcommand('ffmpeg -framerate 25 -i {$image} -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p {$out}', \@_, \%vars);
}

sub capture {
    my $associations = {
        'avi'      => 'ffmpeg -f alsa -i plughw:CARD=PCH,DEV=0 -f video4linux2 -s 640x480 -i /dev/video0 -t 30 {$out}', # mplayer tv:// -tv driver=v4l:width=320:height=300:fps=36:device=/dev/video0 -nosound -frames 1 -vo jpeg # mplayer tv:// -tv driver=v4l2:width=640:height=480:device=/dev/video0 -frames 1 -vo png -vf screenshot # ffmpeg -f video4linux2 -s 640x480 -i /dev/video0 -ss 0:0:2 -frames 1 /tmp/out.jpg # mencoder tv:// -tv driver=v4l2:width=800:height=600:device=/dev/video0:fps=30 -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=1800 -o output.avi
        'jpg'      => \&window2img,
        'mp3'      => 'ffmpeg -f alsa -i hw:0,0 -acodec libmp3lame {$out}',
        'png'      => \&window2img,
        'speakers' => '',
        'wav'      => 'arecord {$out}',
    };
    my $format = shift;
    my $filename = shift;
    $format = $format || 'png';
    ########  !!!!!!!  $filename = $filename || "${\(fsrandomname)}.$format";
    my @args = ( $format, $filename, @_ );
    debugmsg("Capturing format $format into file: $filename");
    debugmsg(Dumper(@args));
    my $command = lookforcommand($associations, 'echo Capturing to format {$format} not implemented', $format);
    my %vars = (
        'format' => qq("$format"),
        'out'    => qq("$filename"),
    );
    return runcommand($command, \@args, \%vars);
}

sub dvdplay {
    #$xmlfile has the following format:
    # <dvdauthor>
    #  <vmgm />
    #   <titleset>
    #     <titles>
    #       <pgc>
    #         <vob file="myvideo.mpg"  chapters="0,0:10,0:20"/>
    #       </pgc>
    #     </titles>
    #   </titleset>
    # </dvdauthor>
    system "mplayer dvd:// -dvd-device dvd";
}

sub dvdcompose {
    #$ mkdir ~/dvd
    #$ cd ~/movie_name/VIDEO_TS
    #$ dvdauthor -t -o ~/dvd *.VOB

    #$ cd ~/dvd/VIDEO_TS
    #$ dvdauthor -o ~/dvd -T
    ## mkisofs -dvd-video -udf -o ~/dvd.iso ~/dvd # if a single title was extracted
    my ($xmlfile) = @_;
    system "dvdauthor -o dvd -x $xmlfile";
}

sub dvdinfo {
    system "dvdbackup -i /dev/sr0 -I";
}

sub dvdbackup {
    system "dvdbackup -i /dev/sr0 -o ~ -t X";
}

sub mmaddindex {
    #system("ffmpeg -i input.mkv -vf "scale=iw/2" half_the_frame_size.mp4"
    my ($infile, $outfile) = @_;
    system("mencoder -idx $infile -ovc copy -oac copy -o $outfile");
}

# Date MP4
# ffprobe -v quiet VID_0041.3gp  -print_format json -show_entries stream_tags:format_tags

sub mmgettags {
    my $associations = {
        'mp3' => 'ffprobe -hide_banner {$in}', # mplayer -vo null -ao null -frames 0 -identify ${videofile}
        'mpg' => 'ffprobe -hide_banner {$in}', # mplayer -vo null -ao null -frames 0 -identify ${videofile}
        'gif' => 'identity -verbose {$i}',
        'jpg' => 'identity -verbose {$i}', # exif $pic | grep Date | grep Origi | cut -d\| -f 2 | cut -d\  -f 1) # convert "$pic" -print "%z\n" ${NULL}
        'ogg' => 'vorbiscomment -l {$in}',
#}
        'png' => 'identity -verbose {$in}',
    };
    my ($filename) = @_;
    my $format = filetype($filename);
    my $command = lookforcommand($associations, 'echo Get info for format {$format} not implemented', $format);
    my %vars = (
        'in'  => qq("$filename"),
    );
    return runcommand($command, \@_, \%vars);
}

sub mmsettags {
    # album
    # artist
    # cd
    # comment
    # date
    # genre
    # title
    # tracknumber
    my $associations = {
        'avi' => '',
        'mp3' => 'ffmpeg -i {$in} {$tags} -y -c:a copy tagged{$in}',
        'ogg' => 'vorbiscomment -a {$in} {$tags}',
        'wav' => '',
    };
    my $tagsformat = {
        'mp3' => "-metadata",
        'ogg' => "-t",
    };
    my ($filename) = @_;
    shift;
    my $format = filetype($filename);
    my $pre = $tagsformat->{$format};
    my @args = map { "$pre \"$_\"" } @_;
    my $args = join(" ", @args);
    my $metadata = {
        'in'   => qq("$filename"),
        'tags' => qq("$args"),
    };
    my $command = lookforcommand($associations, "echo Set info for format {$format} not implemented", $format);
    return runcommand($command, $args, $metadata);
}

#sub font_color {
#    my ($self) = @_;
#    #${IDENTIFY} -list font
    #    #${IDENTIFY} -list color
#}

sub img_resize {
    my ($imgin, $imgout, $percentage) = @_;
    system "convert $imgin -resize $percentage% $imgout";
}

sub imgbanner {
    my ($banner, $filename) = @_;
    system "magick -size 320x85 xc:transparent -font Bookman-DemiItalic -pointsize 36 -draw \"text 25,60 \'$banner\'\" -channel RGBA -blur 0x6 -fill darkred -stroke magenta -draw \"text 20,55 \'$banner\'\" $filename";
}

sub imgtext {
    my ($text, $filename) = @_;
    system "magick -gravity center -font Garamond-Normal -fill red -point-size 26 -draw \"text 0,-100 \'$text\'\" penguin.png $filename";
}

sub loop2jack {
    my ($self) = @_;
    # loop client creation
    #/usr/bin/alsa_out -j ploop -dploop -q 1 $NULL
    #/usr/bin/alsa_in -j  cloop -dcloop -q 1 $NULL

    # give it some time before connecting to system ports
    #sleep 1

    # cloop ports -> jack output ports
    #jack_connect cloop:capture_1 system:playback_1
    #jack_connect cloop:capture_2 system:playback_2

    # system microphone (RME analog input 3) to "ploop" ports
    #jack_connect system:capture_3 ploop:playback_1
    #jack_connect system:capture_3 ploop:playback_2
}

sub rename_tracks {
    my ($self) = @_;
    # Renames tracks based on its position in album structure
    # assumes format: ./${ARTIST}/${ALBUM}/${NN}.${TITLE}.ogg"
    #for title in *.ogg ; do
    #    artist=$(cd .. ; basename $(pwd))
    #    album=$(basename $(pwd))
    #    number=$(echo ${title} | cut -d\. -f 1)
    #    title=$(echo ${title} | cut -d\. -f 2)
    #    ext=$(echo ${title} | cut -d\. -f 3)
    #    mv ${title} "${artist}-${album}-${number}-${title}.$ext"
    #done
}

sub concatenateogg {
    my ($self) = @_;
    #${MENCODER} -oac copy -o newfile.ogg oldfile1.ogg oldfile2.ogg...

    # Unfortunately ${MENCODER} cannot be used without a video stream.
    # There is no -ovc null or something like that.

    #-vc null or -novideo
}

sub extractinterval {
    my ($self) = @_;
    #local file="$1"
    #${MENCODER} -ss 00:30:00 -endpos 00:00:05 -oac copy -ovc copy $file -o $file.out
}

sub play_cd {
    my ($self) = @_;
    #${MPLAYER} -cdrom-device /dev/cdrom cdda://n
}

sub copy_cd {
    my ($self) = @_;
    # To copy an audio CD in the most accurate way, first run
    #${CDDA2WAV} dev=/dev/hdc  cddb=0 -B -O wav
    #${CDRDAO}
    #${CDRDAO} copy --source-device /dev/hdc --device /dev/hdd
    # and then run
    #${CDRECORD} dev=/dev/hdc -v -dao -useinfo -text  *.wav
}

sub burn_cd {
    my ($self) = @_;
    # cdrecord dev=/dev/hdc driveropts=burnfree -v
    # IMG_SIZE=mkisofs -R -q -print-size collection
    # mkisofs -r -joliet -o image.iso collection
    # mount -t iso9660 -o ro,loop=/dev/loop0 image.iso /mnt/cdrom
    # ó
    # mount -t iso9660 -o loop image.iso /mnt/cdrom
    # cdrecord
    # shell> IMG_SIZE=`mkisofs -R -q -print-size private_collection/  2>&1 \
    # | sed -e "s/.* = //"`
    # shell> echo $IMG_SIZE
    # shell> [ "0$IMG_SIZE" -ne 0 ] && mkisofs -r  private_collection/  \
    # |cdrecord  speed=2  dev=0,6,0
    # tsize=${IMG_SIZE}s  -data  -
    #        don't forget the s --^         ^-- read data from STDIN
    # cdrecord speed=12 dev=/dev/hdc -driveropts=burnfree -data image.iso
    # blank=all  # Blanks a CD/RW
}


sub youtube_stream {
    my ($self) = @_;
    #var youtube_url="http://www.youtube.com/watch?v=itvM6UMyFqg"
    ##${MPLAYER} -fs $(echo "http://youtube.com/get_video.php?$(curl -s $youtube_url | sed -n "/watch_fullscreen/s;.*(video_id.+)&amp;title.*;1;p")")
    ##echo "http://youtube.com/get_video.php?$(curl -s $youtube_url | sed -n "/watch_fullscreen/s;.*(video_id.+)&amp;title.*;1;p")"
}

sub scan {
    my ($self) = @_;
    #scanimage -d plustek:libusb:001:014 --resolution 300 --mode Color --format tiff -l 0 -t 0 -x 215mm -y 297mm | convert hoja${i}.pdf
}

sub gethttp {
    my ($self) = @_;
    $ua->agent("Mozilla 5.0 (Linux ) Gecko/20110605 Firefox/3.6.18");
    my $req = HTTP::Request->new(GET => shift);
    my $res = $ua->request($req);
    join("\n",split(/\n/, $res->content )),"\n";
}

sub get_resource {
    my ($self) = @_;
    #local resource="${1}"
    #local destination=$([[ -n ${2} ]] && ( [[ -d ${2} ]] && echo "--directory-prefix ${2}" || echo "-O ${2}" ))
    #local username=${3:+--user ${3}}
    #local password=${4:+--password ${4}}

    #wget  -q ${destination} ${username} ${password} ${resource}
}

sub web_get {
    my ($self) = @_;
    #local server=${1}
    #local port=${2}
    #printf "GET / HTTP/1.0\r\n\r\n" | nc ${server} ${port}
}

sub serve_file {
    #Use serve_dir from the App::SimpleHTTPServer
    my ($self) = @_;
    #local port=${1}
    #local file=${2}
    #local namedpipe=$(mktemp -u)

    #mkfifo --mode=600 ${namedpipe}
    ## netcat: wait 1s after EOF and then quit
    #nc -l -q 1 -s 127.0.0.1 -p $port < ${namedpipe} &> ${NULL} &
    ## send the HTTP headers, followed by a blank line.
    #echo "HTTP/1.1 200 OK" >> ${namedpipe}
    #echo -n "Content-type: " >> ${namedpipe}
    #file -bni ${file} 2> ${NULL} >> ${namedpipe}
    #echo >> ${namedpipe}
    ## Send the file
    #cat ${file} >> ${namedpipe} &
}

sub dossh1 {
    my ($host, $user, $pass, $port, $cmd) = @_;
    $user = $user || "chema";
    $port = $port || 22;
    $ssh2->connect($host, $port) or $ssh2->die_with_error;
    $ssh2->check_hostkey(LIBSSH2_HOSTKEY_POLICY_ADVISORY) or $ssh2->die_with_error;
    if ( $pass ) {
        $ssh2->auth_password($user, $pass) or $ssh2->die_with_error;
    } else {
        $ssh2->auth_agent($user) or $ssh2->die_with_error;
    }
    my $chan = $ssh2->channel();
    $chan->blocking(1);
    $chan->exec($cmd);
    while (<$chan>) {
        okmsg("$_");
    }
    $ssh2->disconnect();
}

sub dossh2 {
    my ($user, $pass, $host) = @_;
    my ($out, $err, $exit);
    my $ssh = Net::SSH::Perl->new($host,
        'options' => [ "StrictHostKeyChecking yes" ]);
    $ssh->login($user, $pass);
    ($out, $err, $exit) = $ssh->cmd("id");
    okmsg("Exit code is $exit");
}

sub dossh3 {
    my ($host, $port, $cmd) = @_;
    my $ssh2 = Net::SSH2->new();
    $ssh2->connect($host, $port ) or $ssh2->die_with_error;
    $ssh2->check_hostkey() or $ssh2->die_with_error;
    $ssh2->auth_agent("theuser") or $ssh2->die_with_error;
    my $chan = $ssh2->channel();
    $chan->blocking(1);
    $chan->exec($cmd);
    while (<$chan>) {
        print;
    }
    #if ($ssh2->auth_keyboard('fizban')) {
    #my $chan = $ssh2->channel();
    #$chan->exec('program');
    #
    #my $sftp = $ssh2->sftp();
    #my $fh = $sftp->open('/etc/passwd') or $sftp->die_with_error;
    #print $_ while <$fh>;
    #}
}


sub symmetric_send_file {
    my ($file, $pass, $dest, $port) = @_;

    system "gpg --symmetric --armor --batch --passphrase $pass --output - $file | nc -c $dest $port";
}

sub symmetric_recv_file {
    my ($port, $pass, $file) = @_;

    system "nc -l -p $port | gpg --batch --passphrase $pass > $file";
}

sub asymmetric_send_file {
    my ($recipient, $file, $dest, $port) = @_;

    system "gpg --encrypt --armor --trust-model always --recipient $recipient --output - $file | nc -c $dest $port";
}

sub asymmetric_recv_file {
    my ($port, $file) = @_;

    system "nc -l -p $port | gpg --output $file";
}

sub clproxy {
    my ($self) = @_;
    # This is a simple command line proxy based on netcat.
    # A bunch of utils can be built arround this tool just by creating the
    # proper script to do the job.
    # If the command to be launched begins with './' it adds listening
    # address and port at the end of command line.

    # Some samples
    # Counts the lines a client type.
    #   ./clproxy.sh localhost 8000 'cat -n'
    # Gimme a shell
    #   ./clproxy.sh localhost 8000 '/bin/sh'

    # To shutdown, either kill netcat listener or wait PROXYTO seconds.

    #CLMARK="`uname -s`"
    #USE="Usage: $0 <host|addr> <port> <command>"

    #NETCAT="netcat"
    #DEBUG=0
    #LOGFILE="$0.log"
    ## Proxy lifetime after last connection (secs).
    #PROXYTO=900
    #HOST=$1
    #ADDR=$HOST
    #shift
    #PORT=$1
    #shift
    #CLINE=$1
    #CLIPAR=$CLINE
    #NCOPTS="-vv -w $PROXYTO -p $PORT"

    #test ! -x "$NETCAT"  && echo "invalid netcat binary: $NETCAT" && exit 1
    #test ! "$HOST" -o ! "$PORT" -o ! "$CLINE" && echo $USE && exit 2
    #if ! test "$CLINE" = "$CLMARK" ; then
    #    # Proxy part
    #    echo "Setting up proxy..."
    #    test -f $LOGFILE && rm -f $LOGFILE
    #    ADDR=`$NETCAT -v -w 1 -z $HOST $PORT 2>&1 | grep '\[' | sed -e 's/.*\[\(.*\)\].*/\1/'`
    #    test ! "$ADDR" && echo "Can't bind to host $HOST" && exit 4
    #else
    #    shift
    #fi
    #CLINE="$*"
    #test ! "$CLINE" && echo $USE && exit 3
    #$NETCAT $NCOPTS -l -s $ADDR -e "$0 $ADDR $PORT \"$CLMARK\" \"$CLINE\"" >> $LOGFILE 2>&1  &
    #test ! "$CLIPAR" = "$CLMARK" && echo "Logging connections to $LOGFILE. Proxy is listening at $ADDR:$PORT." \
    #                             && echo "`date` - Accepting connections..." > $LOGFILE && exit 0
    ## Client part
    #LOG=${NULL}
    #test "$DEBUG" = "1" && LOG=$LOGFILE  && echo "Procesing client.." >> $LOG && echo "Input is: $line" >> $LOG \
    #                    && echo -n "Output is: " >> $LOG
    ## Adds address and port to end of command line whenever executing somethinf like ./
    #XSTUFF="$ADDR $PORT"
    #test "`echo $CLINE | sed -e '/^\.\//d'`" && XSTUFF=""
    #$CLINE $XSTUFF 2>&1 | tee -a $LOG
}

sub port_scanner {
    my ($self) = @_;
    # Simple netcat based portmap scanner.

    #USE="Usage: $0 <host> [ <port> | [ <port> ]-[ <port> ] ]"
    #NETCAT="netcat"
    #FIRSTPORT=1
    #LASTPORT=65535
    #test ! "$1" && echo $USE && exit 1
    #HOST=$1
    #PORT=$2
    #if test `echo "$PORT" | grep "^[0-9]*-[0-9]*$"` ; then
    #    BPORT=`echo "$PORT" | cut -d- -f1`
    #    EPORT=`echo "$PORT" | cut -d- -f2`
    #    test ! "$BPORT" && BPORT=$FIRSTPORT
    #    test ! "$EPORT" && EPORT=$LASTPORT
    #else
    #    if test "$PORT" ; then
    #        BPORT=$PORT  && EPORT=$PORT
    #    else
    #        BPORT=$FIRSTPORT && EPORT=$LASTPORT
    #    fi
    #fi
    #BPORT=`echo "$BPORT" | grep "^[0-9][0-9]*$"`
    #EPORT=`echo "$EPORT" | grep "^[0-9][0-9]*$"`
    #test ! \( "$BPORT" -a "$EPORT" \) && echo "Invalid port(s): $PORT" && echo $USE && exit 2
    #test "$BPORT" -lt "$FIRSTPORT" -o "$EPORT" -gt $LASTPORT && echo "Invalid port(s): $PORT" && echo $USE && exit 2
    #echo $BPORT-$EPORT
    #for ((i=$BPORT ; $i - EPORT - 1; i+=1)) ; do
    #    # Scan the tcp port
    #    $NETCAT -v -z $HOST $i
    #    # Scan the udp port
    #    $NETCAT -v -z -u $HOST $i
    #done
}

sub simple_game {
    my ($self) = @_;
    # ./clproxy.sh <host> <port> './sgame.sh'

    #SECRET=`echo -e "\064\067"`
    #NOCLUE="==Nobody has found the number yet!=="
    #WINNERFILE=$0.win
    #DELAY=10

    #echo "Welcome to this game. You have to guess a number."
    #echo "First player to do so, wins!!!"
    #echo "Please enter your name."
    #read NAME

    #while test 1 ; do
    #   if test ! -r $WINNERFILE ; then
    #      echo $NOCLUE
    #   else
    #      WINNER=`cat $WINNERFILE`
    #      if test ! "$WINNER" ; then
    #         echo $NOCLUE
    #      else
    #         echo "== $WINNER found the number: $SECRET ==" \
    #             && echo "Wait $DELAY secs to play again!!!" && exit 0
    #      fi
    #   fi
    #   while test ! "$GUESS" ; do
    #      echo "Your guess?"
    #      read GUESS
    #      GUESS=`echo $GUESS | grep '^[0-9][0-9]*$'`
    #      test ! "$GUESS"  && echo "That's not a number, mate"
    #   done
    #   if test $GUESS -gt $SECRET ; then
    #      echo "Number is lower"
    #   else
    #      if test $GUESS -lt $SECRET ; then
    #         echo "Number is higher"
    #      else
    #         echo $NAME > $WINNERFILE
    #         echo "== $NAME Wins! == "
    #         (sleep $DELAY ; rm -f $WINNERFILE ) &
    #         break
    #      fi
    #   fi
    #   GUESS=''
    #done
}

sub tinychat {
    my ($self) = @_;
    # Minimal chat system.
    #
    # TODO Some chat rooms....
    #      Save chat log

    #CHATFILE=$0.chat
    #TITLE="===="

    #sub readit {
    #    input=''
    #    while ! test "$input" ; do
    #        echo "$1"
    #        read input
    #    done
    #}
    #touch $CHATFILE
    #while test ! "$name" ; do
    #   readit "I need your nick to enter the chat"
    #   name=$input
    #   test `grep "$name" $CHATFILE` && echo "$name already in the chat" && name=''
    #done
    #echo $name >> $CHATFILE
    #MIMSG=$0.chat.$name
    #while test 1 ; do
    #    touch $MIMSG
    #    echo -e "\n$TITLE Your messages $TITLE"
    #    cat $MIMSG
    #    rm -f $MIMSG
    #    echo -e "\n$TITLE People in $TITLE"
    #    cat $CHATFILE
    #    echo "Message to (<nothing> for all, 'v' for view, 'e' to exit): "
    #    read peer
    #    test "$peer" = "e" && echo "Bye." && break
    #    test ! "$peer" && peer=`cat $CHATFILE`
    #    if test "$peer" != "v" ; then
    #        readit "Text: "
    #        text=$input
    #        for i in $peer ; do
    #            echo "`date` From $name: $text" > $0.chat.$i
    #            echo "Message sent!"
    #        done
    #    fi
    #done
    #cat $CHATFILE | grep -v $name > $CHATFILE
}

sub webserver {
    my ($self) = @_;
    # This is not apache at all, but still can serve some folders and files.
    # It accepts only GET http command. Never tried with IE (good luck).

    # Sample use: ./clproxy.sh host 80 './webserver.sh'

    #ME=`basename $0`
    #ADDR=$1
    #PORT=$2
    #CODE=0
    #INDEX=index.html

    #read request
    ## Tune uri variable below to your browser needs
    #uri=`echo $request | sed -n -e 's/GET \(.*\) HTTP\/1\.[0-1]\r/\1/p'`

    #if test ! "$uri" ; then
    #   CODE=400
    #   MSG="Bad Request"
    #   MORE="Your browser sent a request that this server could not understand.<P>"
    #else
    #    # First try index
    #    if test "${uri:$((${#uri}-1)):1}" = "/" ; then
    #        if test -r ".${uri}${INDEX}" ; then
    #            uri="${uri}${INDEX}"
    #        else
    #            uri="${uri}."
    #        fi
    #    fi
    #    lri=".$uri"
    #    if test ! -r "$lri" {
    #        CODE=404
    #        MSG="Not found"
    #        MORE="The requested URL $uri was not found on this server.<BR>Invalid URI in request $request.<P>"
    #    } else {
    #        if test -d "$lri" {
    #            # Make a nice Index
    #            CODE=''
    #            MSG="Index of $uri"
    #            BASEDIR=`pwd`
    #            test "$uri" != "/" -a "$uri" != "/." && parent=`cd $lri/.. ; cur=\`pwd\` ; echo "${cur#$BASEDIR}/"`
    #            list=`ls -lrth $lri | sed -e 's/-/f/' -e '/^[^df]/d' | \
    #            awk -v addr="$ADDR" -v port="$PORT" -v url="$uri" -v pre="$parent" \
    #            'function href (res) { \
    #            return "http://"addr":"port res } \
    #                BEGIN { pad="                          "  ; \
    #                    if (pre)  \
    #                        printf "d <A HREF=\"%s\">Parent Directory</A>\n",href(pre) \
    #                } \
    #                { printf "%s <A HREF=\"%s\">%s</A>%s%14s %-5s% s%15s\n", \
    #                  substr($1,1,1),href(url"/"$9),$9,substr(pad,length($9)) \
    #                     ,$6,$7,$8,$5  \
    #                }'`
    #            MORE="<HR><PRE>$list</PRE>"
    #         } else {
    #            # Serve the file
    #            if test -s $lri {
    #                exec cat $lri
    #            } else {
    #                MSG="Zero byte file"
    #            }
    #        }
    #    }
    #}
    #            exec cat << EOT
#<HTML><HEAD>
#<TITLE>$CODE $MSG</TITLE>
#</HEAD><BODY>
#<H1>$MSG</H1>
#$MORE
#<HR>
#<ADDRESS>$ME at $ADDR Port $PORT</ADDRESS>
#</BODY></HTML>
#EOT
}

sub create_bridge {
    my ($self) = @_;
    #ip link add name br0 type bridge
    #ip link set bridge br0 up
    #ip link set enp1s0 up
    #ip link set enp1s0 master br0
    #dhcpd
}


sub host_adapter {
    my $interface = $1;
    my $host_interface = "vboxnet0";
    if (!$interface ) {
        print "No interface given (eg. wlp2s0, enp3s0, ...)"
    } else {
        #iptables -A FORWARD -o ${interface} -i ${HOST_INTERFACE} -s ${HOST_NETWORK} -m conntrack --ctstate NEW -j ACCEPT
        #iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
        #iptables -t nat -F POSTROUTING
        `sudo iptables -t nat -A POSTROUTING -o $interface -j MASQUERADE`;
        `sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward`;
    }
}

sub weather {
    my $location = join "_", @_;
    system "curl -s --connect-timeout 3 -m 5 http://wttr.in/$location";
}


sub sendmail {
    # TBI
    #    my ($from, $to, $subject, $message, $smtp, $port, $user, $pass, $cc, $bcc) = @_;
    #echo "$msg" | mail \
    #-r "$from" \
    #-c "$cc" \
    #-b "$bcc" \
    #-a "$attachment" \
    #-s "$subject" \
    #-S v15-compat \
    #-S mta=smtp://$user:$pass@$smtp:$port \
    #-S smtp-use-starttls \
    #-S tls-verify=ignore \
    #$to
}

sub emailattachment {
    my ($MESSAGE) = @_;
    my $parser = MIME::Parser->new( );

    # don't write attachments to disk
    $parser->output_to_core(1);
    # OR
    # parsing creates files like /tmp/msg-1048509652-16134-0/foo.png
    #$parser->output_under("/tmp");
    # OR
    # parsing creates files like /tmp/foo.png
    #$parser->output_dir("/tmp");

    # Cleanup files once done
    #$parser->filer->purge;

    my $message  = $parser->parse_data($MESSAGE);    # die( )s if can't parse
    # OR
    #$message  = $parser->parse($FILEHANDLE);      # die( )s if can't parse

    my $head     = $message->head( );                 # object--see docs
    my $preamble = $message->preamble;               # ref to array of lines
    my $epilogue = $message->epilogue;               # ref to array of lines

    my $num_parts = $message->parts;
    for (my $i = 0; $i < $num_parts; $i++) {
      my $part         = $message->parts($i);
      my $content_type = $part->mime_type;
      my $body         = $part->as_string;
    }

    for (my $i = 0; $i < $num_parts; $i++) {
      my $part = $message->parts($i);
      my $fh = $part->open("r") or die "Can't open for reading: $!\n";
      while (<$fh>) {
        # reading lines from the current attachment
        # ...
      }
   }
}

sub repos {
    my ($action) = @_;
    debugmsg("Action: $action");
    my $command;
    my $actions = {
        'list' => {
            'msg'     => "Listing repos in current dir",
            'dir'     => ".",
            'command' => "grep 'url' .git/config | cut -d= -f2 | cut -d' ' -f2",
        },
        'status' => {
            'msg'     => "Status of your repositories",
            'dir'     => "$MYREPOSPATH",
            'command' => "git status --short",
        },
        'update' => {
            'msg'     => "Updating foreign repositories",
            'dir'     => "$EXTREPOSPATH",
            'command' => "git pull",
        },
        'update-mine' => {
            'msg'     => "Updating my repositories",
            'dir'     => "$MYREPOSPATH",
            'command' => "git pull",
        },
    };
    $command = $actions->{$action} if $action;
    debugmsg("Command: " . Dumper($command));
    unless ($command) {
        okmsg("Command: " . join( ",", keys %$actions ));
    }
    else {
        boldmsg "$actions->{$action}->{msg}";
        my $dir = "$actions->{$action}->{dir}/";
        $command = $actions->{$action}->{command};
        for (`find $dir -maxdepth 1 -mindepth 1 -type d`) {
            debugmsg("Current object: $_");
            chomp;
            okmsg(basename("$_")) unless $action eq "list";
            system "(cd $_ ; $command)";
        }
    }
}

sub lispupdate {
   print "sbcl --eval '(progn (ql:update-dist \"quicklisp\") (ql:update-client) (quit))'";
   system "sbcl --eval '(progn (ql:update-dist \"quicklisp\") (ql:update-client) (quit))'";
}

sub fullupdate {
   okmsg("Performing full update");
   okmsg("Updating ArchLinux...");
   run_as_root("pacman -Suy");
   okmsg("Updating CPAN...");
   system "perl -MCPAN -e upgrade";
   #okmsg "Updating Ruby gems";
   #system "gem update";
   okmsg("Updating QuickLisp and Lisp ASDF packages...");
   lispupdate();
   #okmsg("Updating Moodle...");
   #update_moodle_tree("http", "/srv/http/moodle");
   okmsg("Updating Repos...");
   repos("update");
   okmsg("Updating MY Repos...");
   repos("update-mine");
   okmsg("Status of my repos:");
   repos("status");
}

sub postfix_queued_mail {
    my ($postfixuser) = @_;
    # mailq can be used instead
    run_as_user($postfixuser, "postqueue -p");
}

sub postfix_show_queued_msg {
    my ($postfixuser, $msgid) = @_;
    #local msgid="${1}"

    run_as_user($postfixuser, "postcat -vq $msgid");
}

sub postfix_process_queued_mail {
    my ($postfixuser) = @_;
    # "postfix flush" can be used instead
    run_as_user($postfixuser, "postqueue -f");
}

sub postfix_delete_queued_mail {
    my ($postfixuser, $typeofmail) = @_;
    # Type of mail: deferred, ...

    run_as_user($postfixuser, "postsuper -d ALL $typeofmail");
}


sub postgresql_initdb {
    my ($pathtocluster) = @_;
    system "initdb -D $pathtocluster";
}

sub postgresql_createdb {
    my ($dbname, $description) = @_;
    system "createdb -U postgres -E unicode -O owner -W $dbname \"$description\"";
}

sub postgresql_dropdb {
    my ($self) = @_;
    system "dropdb mydb";
}

sub postgresql_createuser {
    my ($self) = @_;
    # -d new user will be allowed to create database
    system "createuser -a -d -P -E -U postgres -W master";
}

sub postgresql_createotheruser {
    my ($self) = @_;
    #-D no create database
    #-P prompt for new pass
    #-E encrypt pass
    #-U user to connect as
    system "createuser -D -P -E -U master -W user";
}

sub postgresql_dropuser {
    my ($self) = @_;
    system "dropuser user";
}

sub postgresql_changepasswd {
    my ($user, $password) = @_;
    system "alter user $user with password 'password'";
}

sub postgresql_listdbs {
    my ($self) = @_;
    # Inside psql type \l
    system "psql -l";
}

sub postgresql_listobjs {
    my ($self) = @_;
    # Inside psql type \d
    #echo "TBI"
}

sub postgresql_language {
    my ($self) = @_;
    # Procedural languages
    system "createlang -U postgres plpgsql mydb";
}

sub postgresql_startdb {
    my ($self) = @_;
    system "/usr/bin/postmaster -D /var/lib/postgresql/data";
    system "/usr/bin/pg_ctl -D /var/lib/postgresql/data -l logfile start";
}

sub postgresql_backupdb {
    my ($db, $file) = @_;
    # -O No owner
    # -F c : Format suitable for pg_restore
    # -b : Include large objects
    run_as_user("postgres", "pg_dump -F c -b -f ${file} ${db}");
}

sub postgresql_restoredb {
    my ($self) = @_;
    system "pg_restore  -d database database.bak";
}

sub postgresql_updatedb {
    my ($self) = @_;
    system "pg_dumpall -p 5432 | psql -d postgres -p 6543";
}

sub update_moodle_tree {
    my ($moodleuser, $moodledir) = @_;
    run_as_user($moodleuser, "git -C $moodledir pull");
}

# PENDING. Fonts
# xset +fp /usr/share/fonts/inconsolata
# xset fp rehash

sub Xupdateresources {
    my ($self) = @_;
    system "xrdb -merge ~/.Xresources";
}

sub Xxrandr {
    my ($self) = @_;

    system "xrandr --output HDMI1 --off --output VIRTUAL1 --off --output DP1 --off --output eDP1 --primary --mode 1024x768 --pos 0x0 --rotate normal --output VGA1 --mode 1024x768 --pos 0x0 --rotate normal";
    system "xrandr --output VGA1 --mode 1280x1024"
}

sub Xbrowsersync {
    my ($browser) = @_;
    # whether to use SHIFT+CTRL+R to force reload without cache
    my $reloadkeys = "CTRL+R";
    $reloadkeys = "SHIFT+CTRL+R";
    $browser = $browser || "Mozila Firefox";
    # get which window is active right now
    my $currentwindow = `xdotool getactivewindow`;
    # bring up the browser
    system "xdotool search --name $browser windowactivate --sync";
    # send the page-reload keys (C-R) or (S-C-R):
    system "xdotool search --name $browser key --clearmodifiers $reloadkeys";
    # sometimes the focus doesn't work, so follow up with activate
    system "xdotool windowfocus --sync $currentwindow";
    system "xdotool windowactivate --sync $currentwindow";
}

# TODO: Used?
sub umount_media {
    my $mediamount = "/run/media";
    debugmsg("Unmounting all media mounts: $mediamount");
    my @mounts = `df -h | grep "$mediamount"`;
    debugmsg("Umounting: @mounts");
    my @mount;
    my $mount;
    debugmsg(Dumper(@mounts));
    foreach (@mounts) {
        @mount = split / +/, $_;
        $mount = $mount[5];
        okmsg("Unmouting $mount");
        umount($mount);
    }
}

sub mount_nut {
    my ($mountpoint, $prefix) = @_;
    $mountpoint = $mountpoint || $NUT_MOUNTPOINT;
    mydie("$mountpoint is not a directory") if not -d $mountpoint;
    $prefix = $prefix || "";
    my $nut_vg="$prefix$NUT_VG_NAME";
    lvup($nut_vg, $NUT_LV_NAME);
    # TODO: Use concatenation
    my $luks_device = luks_open("/dev/$nut_vg/$NUT_LV_NAME");
    debugmsg("Mountpoint is: $mountpoint");
    debugmsg("Luks device is: $luks_device");
    return mount("/dev/mapper/$luks_device", $mountpoint);
}

sub umount_nut {
    my ($mountpoint, $prefix) = @_;
    $mountpoint = $mountpoint || $NUT_MOUNTPOINT;
    $prefix = $prefix || "";
    my $nut_vg = "$prefix$NUT_VG_NAME";
    my $luks_device = ismounted($mountpoint);
    if ($luks_device) {
        umount($mountpoint);
        luks_close($luks_device);
        return vgdown($nut_vg);
    } else {
        return 0;
    }
}

sub sync_nut {
    my $dirstoexclude = "lost+found,blib,.git";
    mount_nut() or mydie("Unable to mount nut");
    okmsg("Syncing nut, excluding dirs $dirstoexclude");
    sync("~/$MYRC_DIR/", "$NUT_MOUNTPOINT/$MYRC_DIR/", $dirstoexclude);
    umount_nut();
}

sub backup_nut {
    my ($backupfile) = @_;
    $backupfile = $backupfile || "";
    mydie ("Invalid backup file: $backupfile") if ! -f $backupfile;
    mount_nut() or mydie("Unable to mount nut");
    debugmsg("Backup file is: $backupfile");
    my $backuploopdevice = loopdeviceopen($backupfile);
    debugmsg("Backup loop device is: $backuploopdevice");
    my $luksdevice = luks_open($backuploopdevice);
    $luksdevice = "/dev/mapper/$luksdevice";
    my $backupdir = maketempdir();
    debugmsg("LUKS device is: $luksdevice");
    debugmsg("Backup dir is: $backupdir");
    mount($luksdevice, $backupdir) or mydie("Unable to mount nut backup");
    debugmsg("Syncing $NUT_MOUNTPOINT/ -> $backupdir/");
    sync("$NUT_MOUNTPOINT/", "$backupdir/", "lost+found");
    umount_nut();
    umount($backupdir);
    luks_close($luksdevice);
    loopdeviceclose($backuploopdevice);
    rmdir($backupdir);
}

sub hdmi {
    # NO HDMI:
    #xrandr --output eDP1 --primary --mode 1366x768 --pos 0x0 --rotate normal --output DP1 --off --output HDMI1 --off --output VGA1 --off --output VIRTUAL1 --off
    # HDMI:
    # xrandr --output eDP1 --primary --mode 1366x768 --pos 1440x0 --rotate normal --output DP1 --off --output HDMI1 --mode 1440x900 --pos 0x0 --rotate normal --output VGA1 --off --output VIRTUAL1 --off
    #system "xrandr --output eDP1 --primary --mode 1366x768 --pos 0x0 --rotate normal --output DP1 --off --output HDMI1 --mode 1920x1080 --pos 0x0 --rotate normal --output VGA1 --off --output VIRTUAL1 --off"
    system "xrandr --output eDP1 --primary --mode 1366x768 --pos 0x0 --rotate normal --output DP1 --off --output HDMI1 --mode 1360x768 --pos 0x0 --rotate normal --output VGA1 --off --output VIRTUAL1 --off";
    # pacmd list-cards
    system "pactl set-card-profile 0 output:hdmi-stereo";
}

sub show_errors {
    system "journalctl --no-pager --since today --grep 'fail|error|fatal' --output json | \
            jq '._EXE' | sort | uniq -c | sort --numeric --reverse --key 1";
}

#run_on_change
#
#    See also:
#    while true ;  do
#        atime=`stat -c %Z /path/to/the/file.txt`
#        if [[ "${atime}" != "${LTIME}" ]] ; then
#            echo "RUN COMMNAD"
#            LTIME=${atime}
#        fi
#        sleep 5
#    done
#    #######################
#    path=$1
#    shift
#    cmd=$*
#    sha=0
#    sub update_sha {
#        sha=`ls -lR --time-style=full-iso $path | sha1sum`
#    }
#    update_sha
#    previous_sha=$sha
#    build {
#        echo -en " building...\n\n"
#        $cmd
#        echo -en "\n--> resumed watching."
#    }
#    compare {
#        update_sha
#        if [[ $sha != $previous_sha ]] ; then
#            echo -n "change detected,"
#            build
#            previous_sha=$sha
#        else
#            echo -n .
#        fi
#    }
#    trap build SIGINT
#    trap exit SIGQUIT
#    echo -e "--> Press Ctrl+C to force build, Ctrl+\\ to exit."
#    echo -en "--> watching \"$path\"."
#    while true; do
#        compare
#        sleep 1
#    done

sub run_on_change {
    my ($object, @args) = @_;
    my $command = join " ", @args;
    $object = $object || ".";
    my $path = filepath($object);
    $command = $command || "date";
    debugmsg("Changing to $path. Waiting with: inotifywait -r -e close_write,moved_to,create,delete $object. Running: $command");
    system "( cd $path ; inotifywait -r -e close_write,moved_to,create,delete $object ; $command )";
}

sub watch_and_run {
    my ($path, @args) = @_;
    $path = $path || ".";
    my $command = join " ", @args;
    $command = $command || "ls";
    debugmsg("Path is $path. Command is $command");
    while (1) {
        run_on_change($path, $command);
        sleep(1);
    }
}


sub say_fortune {
    #my ($self, $me, @args) = @_;
    my $fortune = join(" ", @_);
    $fortune = `fortune -s` unless $fortune;
    $fortune =~ s/"/'/;
    chomp $fortune;
    my $cow = ( isxterminal() ? "x" : "") . "cowsay";
    system "$cow \"$fortune\"";
}

sub permute {
    my @array = 'a'..'d';
    my $p_iterator = Algorithm::Permute->new ( \@array );

    while (my @perm = $p_iterator->next) {
       okmsg("next permutation: (@perm)");
    }
}

# mytmux
#
#    session="tmux"
#
#    if ! tmux has-session -t ${session} ; then
#        echo "Creating tmux session ${session}"
#        tmux new-session -s ${session} -n gentoo -d
#        tmux send-keys -t ${session} 'gentoo' C-m
#        tmux new-window -n console -t ${session}
#    fi
#    tmux attach -t ${session}
#
#    tmux send-keys -t development 'cd ~/mydi' C-m
#    tmux send-keys -t development 'vim' C-m
#    tmux split-window -v -p 10 -t development
#    tmux select-layout -t development main-horizontal
#    tmux send-keys -t development:1.2 'cd ~/mydi' C-m
#    tmux send-keys -t development:2 'cd ~/mydi' C-m
#    tmux select-window -t development:1


sub mytmux {
    debugmsg("TBI");
}

#sub launch_machines {
#    #ROOTDIR=/media/hd
#    #MACHDIR=${ROOTDIR}/gentoo_vms
#    #
#    #mountpoint -q ${ROOTDIR} && sudo mount -L HD ${ROOTDIR}
#    #tmux new-session -s machines -d -n x86
#    #tmux set-option -g allow-rename off
#    #tmux new-window -t machines -n amd64
#    #tmux send-keys -t machines:x86 ${MACHDIR}/gentoo_x86/gentoo_x86.sh C-m
#    #tmux send-keys -t machines:amd64 ${MACHDIR}/gentoo_amd64/gentoo_amd64.sh C-m
#    #sleep 8
#    #tmux new-session -s clients -d -n x86
#    #tmux set-option -g allow-rename off
#    #tmux new-window -t clients -n amd64
#    #tmux send-keys -t clients:x86 ${MACHDIR}/gentoo_x86/client.sh C-m
#    #tmux send-keys -t clients:amd64 ${MACHDIR}/gentoo_amd64/client.sh C-m
#    #tmux attach -t clients
#}

sub mount_vdi {
    # Install qemu if needed
    # rmmod nbd
    # modprobe nbd max_part=16
    # qemu-nbd -c /dev/nbd0 drive.vdi
    # Now you will get a /dev/nbd0 block device, along with several /dev/nbd0p* partition device nodes.
    # mount /dev/nbd0p1 /mnt
    # Once you are done, unmount everything and disconnect the device:
    # qemu-nbd -d /dev/nbd0
    # Create a vdi format with:
    # vboxmanage createmedium --filename=disk.vdi --size=N (in MB)
    # How to dettach and remove a media from VB:
    # vboxmanage list hdds
    # vboxmanage list vms -l
    # vboxmanage storageattach NAMEOFVM --storagectl "USB" --port 0 --medium none                                          1 ✘
    # vboxmanage closemedium disk UUID

    my ($vdifile, $mountpoint) = @_;
    run_as_root("modprobe nbd max_part=16");
    run_as_root("qemu-nbd -c /dev/nbd0 ${vdifile}");
    mount("/dev/nbd0p1", "${mountpoint}");
}

sub cleansystem {
    run_as_root("paccache -rk1");
    run_as_root("pacman -Rns `pacman -Qtdq`");
    okmsg("Consider removing contents of \$HOME/.config and \$HOME/.local/share");
    okmsg("Consider running rmlint $HOME");
}

sub cpu_usage {
    current_cpu_usage();
}

sub show_my_mounts {
    show_user_mounts(whoami());
}

sub toggle_service {
    my ($service) = @_;
    my $action = "stop";

    mydie("need a service to toggle"), if not $service;
    run_as_root("systemctl is-active --quiet $service");
    $action = "start" if $?;
    run_as_root("systemctl $action $service");
}

#sub toggle_wifi {
#    my $command = "nmcli radio wifi";
#    my $status = `$command`;
#    chomp($status);
#    okmsg("$status");
#    if ($status eq "enabled") {
#        okmsg("Disabling wifi");
#        system("$command off");
#    } else {
#        okmsg("Enabling wifi");
#        system("$command on");
#    }
#    #system "bluetoothctl show | grep -q 'Powered: yes'";
#    #$action = "on" if $?;
#    #debugmsg("bluetoothctl power $action $NULL");
#    #system "bluetoothctl power $action $NULL";
#}

__END__
