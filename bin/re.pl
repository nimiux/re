#!/usr/bin/env perl

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

use Data::Dumper;
use File::Basename;
use Getopt::Long::Descriptive;

use CoreSubs::CoreSubs qw(
    $DEBUG
    debugmsg
    mydie
);

use CoreSubs::Storage qw(
    real_path
);

use Re qw(
);

$SIG{INT} = sub {
    mydie "Caught signal, exiting!";
};

my ( $opt, $usage ) = describe_options(
    "%c %o function arguments",
    [],
    [ 'verbose|v', "show debugging information" ],
    [ 'help|h',    "show help", { shortcircuit => 1 } ],
);

sub usage {
    print( $usage->text );
}

sub function_not_found {
    my ( $functionname, $realname ) = @_;
    say "function $functionname not found. Use: $realname list or $realname list <text>";
}

my @args = @ARGV;
debugmsg( Dumper(@args) );
my ($function) = @args;
my $call = \&usage;

usage(), exit if $opt->help;
$DEBUG = 1 if $opt->verbose;
my $execname = basename($0);
my $realname = basename( real_path($0) );
debugmsg("Execname: $execname. Realname: $realname");
my $printit = 0;
if ( $execname eq $realname ) {
    $function = $args[0] if defined $args[0];
    shift @args;
}
else {
    $function = $execname;
}
debugmsg("Function is $function");
if ( defined $function ) {
    $function =~ tr/-/_/;
    debugmsg("Function is now: $function");
    $call = $Re::{$function};
    if ( not defined $call ) {
        $call    = \&function_not_found;
        $args[0] = $function;
        $args[1] = $realname;
    }
}
debugmsg( "Calling $call with arguments " . Dumper( \@args ) );
my $result = $call->(@args);
debugmsg( "Call dispatched " . Dumper($result) ) if $printit;
say $result                                      if $printit;

__END__

=encoding utf8

=head1 NAME

re.pl - Front-End to Re.pm module

=cut

=head1 SYNOSIS

Re.pm functions can be used by writing a new Perl program and include:
  use Re;
  Re::filename();
Calling Re.pm functions is easy through this program, to do so you can:

1. Use this script writing the function name as its first parameter:
     re say-fortune;
2. Create a symbolic link to this script ann name it as the function to be called:
     $ ln -s wath-and-run /path/to/the/re/script
     $ wath-and-run  # calls the watch_and_run function

It's important to remmember that for the sake of useness, the underscore is translated to hyphens before calling the Re.pm function.
This is because Perl functions cannot contain underscores.

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2025 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/re/issues

=cut
