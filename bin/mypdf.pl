#!/usr/bin/env perl

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

use Data::Dumper;
use Getopt::Long::Descriptive;

Re::convert();
IESTools::showfile();
use CoreSubs::CoreSubs qw(
  $DEBUG
  debugmsg
);

use CoreSubs::Storage qw(
  filename
  filetype
  maketempdir
);

#use Re qw(
#  convert
#);

sub joinpdf {
    my ( $usage, $args ) = @_;
    my $outfile = pop @{$args};
    if ( not defined $outfile ) {
        say "Need output file";
        print( $usage->text );
    }
    else {
        if ( scalar @{$args} eq 0 ) {
            say "Need input file(s)";
            print( $usage->text );
        }
        else {
            my $tempdir = maketempdir;
            my $tempfile;
            my @files;
            debugmsg("Tempdir is $tempdir");
            foreach ( @{$args} ) {
                $tempfile = "$tempdir/${\filename($_)}.pdf";
                debugmsg("Converting file: $_ to pdf into $tempfile");
                convert( $_, filetype($_), "$tempfile", "pdf" );
                push @files, "$tempfile";
            }
            debugmsg( Dumper( \@files ) );
            my $files = join " ", @files;
            debugmsg("Files for pdf: $files");

            # qpdf --empty --pages $files 1-z a2.pdf 1-z -- out.pdf
            system "qpdf --empty --pages $files -- $outfile";
        }
    }
}

sub croppdf {
    # qpdf infile.pdf --pages . 1,35-50,67 -- output.pdf
    debugmsg("TBI");
}


sub splitpdf {
    my ( $usage, $args ) = @_;
    my $infile = shift @{$args};
    if ( not defined $infile ) {
        say "Need input file";
        print( $usage->text );
    }
    else {
        if ( !-e $infile ) {
            say "Can't open file $infile";
            print( $usage->text );
        }
        else {
            my $name  = filename($infile);
            my $pages = `qpdf --show-npages $infile`;
            for my $i ( 1 .. $pages ) {
                debugmsg("qpdf $infile --pages $infile $i -- $name-$i.pdf");
                system("qpdf $infile --pages $infile $i -- $name-$i.pdf");
            }
        }
    }
}

sub viewpdf {
    my ( $usage, $args ) = @_;
    my $infile = shift @{$args};
    if ( not defined $infile ) {
        say "Need input file";
        print( $usage->text );
    }
    else {
        system "mupdf $infile";
    }
}

# TODO. Include this mechanish in coresubs
my %actions = (
    'join'  => \&joinpdf,
    'split' => \&splitpdf,
);

sub dispatch {
    my ( $action, $usage, $args ) = @_;
    debugmsg("Action $action");
    my $func = $actions{$action};
    if ( not $func ) {
        say "Invalid action $action. Valid actions are: " . join " ",
          keys %actions;
    }
    else {
        $func->( $usage, $args );
    }
}

my ( $opt, $usage ) = describe_options(
    "%c %o action files",
    [],
    [ 'verbose|v', "show debugging information" ],
    [ 'help|h',    "show help", { shortcircuit => 1 } ],
);

print( $usage->text ), exit if $opt->help;
$DEBUG = 1 if $opt->verbose;
my @args   = @ARGV;
my $action = shift @args;
print( $usage->text ), exit if not $action;
dispatch( $action, $usage, \@args );

__END__

=encoding utf8

=head1 NAME

mypdf.pl - actions over pdf files

=head1 SYNOSIS

Usage:
mypdf join file1.png file2.pdf file3.gif file4.txt output.pdf
mypdf split input.pdf
mypdf view input.pdf

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2025 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/re/issues

=cut
