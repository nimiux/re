#!/usr/bin/perl

package main;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

use Data::Dumper;
use Getopt::Long;
use Pod::Usage;
use Encode qw(decode encode);
use CAM::PDF;
use File::Spec::Functions qw(
    catfile
);

use Date::Calc qw(
    Month_to_Text
    Today
);

use CoreSubs::CoreSubs qw(
    $DEBUG
    debugmsg
    komsg
    mydie
);

use CoreSubs::Storage qw(
    absolutepath
    filename
    filepath
);


use open qw(:utf8);

my %opts = (
            list         => 0,
            verbose      => 0,
            help         => 0,
            );

sub getfieldlist {
    my ($document) = @_;
    return \$document->getFormFieldList();
}

sub getcurrentdate {
    my ($currentyear, $currentmonth, $currentday) = Today();
    $currentyear = substr($currentyear, -2);
    $currentmonth = Month_to_Text($currentmonth, 4);
    return ($currentyear, $currentmonth, $currentday);
}

Getopt::Long::Configure('bundling');
GetOptions('l|list'          => \$opts{list},
           'v|verbose'       => \$opts{verbose},
           'h|help'          => \$opts{help},
           ) or pod2usage(1);
if ($opts{help}) {
   pod2usage(-exitstatus => 0, -verbose => 2);
}
pod2usage(1) if (@ARGV < 1);

$DEBUG = 1 if $opts{verbose};

my $document = shift;
mydie "$document no es un fichero" if not -f $document;

my $curso = shift;
my $alumno = join " ", @ARGV;
debugmsg(Dumper(@ARGV));
$alumno = decode("utf-8", $alumno);

my $info = catfile(filepath(absolutepath($document)), filename($document)) . ".pl";
debugmsg("Fichero de info: $info");

my $fields = evalperl($info);
debugmsg(Dumper($fields));

my ($currentyear, $currentmonth, $currentday) = getcurrentdate();

my $doc = CAM::PDF->new($document) || mydie "$CAM::PDF::errstr\n";
print(Dumper(getfieldlist($doc))), exit if $opts{list};

my @list;
push @list, 'alumno';
push @list, $alumno;
push @list, 'tutor';
push @list, $fields->{tutor};
push @list, 'ciclo';
push @list, $fields->{ciclo};
push @list, 'curso';
push @list, $curso;
push @list, 'modulo';
push @list, $fields->{modulo};
push @list, 'ano';
push @list, $currentyear;
push @list, 'mes';
push @list, $currentmonth;
push @list, 'dia';
push @list, $currentday;
$doc->fillFormFields({}, @list);
$alumno =~ s/ /_/g;
$doc->cleanoutput("./$alumno-1.pdf");

__END__

=encoding utf8

=head1 NAME

fillpdf.pl - Fills the fields of a PDF document

=head1 SYNOPSIS

 fillpdf.pl [options] document.pdf [ value_for_field_1 value_for_field_2 ... ]

 argument:
   document.pdf   documento to fill in
 options:
   -l --list      list document fields

   -h --help      show help
   -v --verbose   show debugging information
 output:
   ????.pdf       PDF document filled

=head1 DESCRIPTION

 Rellena los campos de un documento PDF

=head1 TODO

This program requires heavy refactoring to fill a PDF based in the name of the fields;

=head1 VERSION

This man page documents IESTools version 1.0.

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2025 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/iestools/issues

=cut
