# !/usr/bin/env perl

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

use Data::Dumper;
use File::Basename;
use Getopt::Long::Descriptive;
use Scalar::Util qw(looks_like_number);

use CoreSubs::CoreSubs qw(
    $DEBUG
    convertbase
    debugmsg
);

use CoreSubs::Storage qw(
    fileext
    filename
    filetype
);
use Re qw(
    convert
);

sub pdf2pages {
    my ($filename) = @_;
    my $name       = filename($filename);
    my $pages      = `qpdf --show-npages $filename`;
    for my $i (1 .. $pages) {
        debugmsg("qpdf $filename --pages $filename $i -- $name-$i.pdf");
        system("qpdf $filename --pages $filename $i -- $name-$i.pdf");
    }
}

sub file2array {
    my ($filename) = @_;
    open my $handle, '<', $filename;
    chomp (my @lines = <$handle>);
    close $handle;
    return \@lines;
}

sub csv2hash {
    debugmsg ("TBI " . join ("|", @_));
}

# TODO. Remove call to describe_options
my ($opt, $usage) = describe_options(
    "%c %o number|format|filename1 base1|filename2 base2",
    [],
    [ 'verbose|v', "show debugging information" ],
    [ 'help|h',    "show help", { shortcircuit => 1 } ],
);

print($usage->text), exit if $opt->help;
$DEBUG = 1 if $opt->verbose;
my @args  = @ARGV;
my $first = shift @args;
print($usage->text), exit if not $first;
if (looks_like_number($first)) {
    # converter number frombase tobase
    my ($inbase, $outbase) = @args;
    print (convertbase($first, $inbase, $outbase));
}
else {
    my ($infile, $informat, $outfile, $outformat);
    debugmsg("first is $first");
    if (-e $first) {

        # converter infile.informat outfile.outformat -> infile.outformat
        debugmsg ("converter infile.informat outfile.outformat -> infile.outformat");
        $infile    = $first;
        $informat  = filetype($first);
        $outfile   = shift @args;
        $outformat = fileext($outfile);
    }
    else {
        # converter outformat infile.informat -> infile.outformat
        debugmsg ("converter outformat infile.informat -> infile.outformat");
        $infile    = shift @args;
        $informat  = fileext($infile);
        $outformat = $first;
        $outfile   = "${\filename($infile)}.$outformat";
    }
    debugmsg ("converter: infile=$infile, informat=$informat, outfile=$outfile, outformat=$outformat");
    convert ($infile, $informat, $outfile, $outformat);

    # Other cases:
    # converter fromformat toformat file.fromformat -> file.toformat
    # conveter format format ...
}

__END__

=encoding utf8

=head1 NAME

converter.pl - Converts between numeric bases and media formats

=head1 SYNOSIS

Usage:

=head1 TODO

Mix a video with a sound file
ffmpeg -i sound.wav -i video.avi video+sound.mpg
Multi-pass encoding with ffmpeg
ffmpeg -i fichierentree -pass 2 -passlogfile ffmpeg2pass fichiersortie-2
system("for i in $(seq 1 $(qpdf --show-npages $filename)); do qpdf $filename --pages $filename $i -- $name-$i.pdf; done");

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2025 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/re/issues

=cut
