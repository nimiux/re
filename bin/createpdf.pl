#!/usr/bin/env perl

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

use Data::Dumper;
use Getopt::Long::Descriptive;
#use File::Basename;
#use Scalar::Util qw(looks_like_number);

use CoreSubs::CoreSubs qw(
    $DEBUG
    debugmsg
);
use CoreSubs::Storage qw(
    filename
    filetype
    maketempdir
);
use Re qw(
    convert
);

sub pdf2pages {
    my ($filename) = @_;
    my $name = filename($filename);
    my $pages = `qpdf --show-npages $filename`;
    for my $i (1..$pages) {
        debugmsg("qpdf $filename --pages $filename $i -- $name-$i.pdf");
        system("qpdf $filename --pages $filename $i -- $name-$i.pdf");
    }
}

sub createpdf {
    my ($outfile, $infiles) = @_;
    my $tempdir = maketempdir;
    my $tempfile;
    my @files;
    debugmsg("Tempdir is $tempdir");
    foreach (@{$infiles}) {
       $tempfile = "$tempdir/${\filename($_)}.pdf";
       debugmsg("Converting file: $_ to pdf into $tempfile");
       convert($_, filetype($_), "$tempfile", "pdf");
       push @files, "$tempfile"
    }
    debugmsg(Dumper(\@files));
    my $files = join " ", @files;
    debugmsg("Files for pdf: $files");
    # qpdf --empty --pages $files 1-z a2.pdf 1-z -- out.pdf
    system "qpdf --empty --pages $files -- $outfile";
}

my ($opt, $usage) = describe_options(
    "%c %o file1 file2 ... filen output.pdf",
    [],
    [ 'verbose|v',  "show debugging information" ],
    [ 'help|h',     "show help", { shortcircuit => 1 } ],
);

print($usage->text), exit if $opt->help;
$DEBUG = 1 if $opt->verbose;
my @args = @ARGV;
my $output = pop @args;
if (not defined $output) {
    say "Need output file";
    print($usage->text);
} else {
    if (scalar @args eq 0) {
        say "Need input file(s)";
        print ($usage->text);
    } else {
        createpdf($output, \@args);
    }
}

__END__

=encoding utf8

=head1 NAME

createpdf.pl - Creates a pdf from multiple files

=cut

=head1 SYNOSIS

Usage:
createpdf file1.png file2.pdf file3.gif file4.txt output.pdf

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2025 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/re/issues

=cut
