#!/bin/sh

# Prerequisites
#  Install cpan
#    It's highly recommendable to configure cpan to use https and verify signatures:
#    $ rlwrap cpan
#      o conf urllist https://www.cpan.org/
#      o conf commit
#      install Module::Signature
#      o conf check_sigs true
#      o conf commit
#      install Crypt:OpenPGP
#  Install the following packages for your distro:
#   inotify-tools
#  Install CoreSubs from the nimiux repo:
#    https://codeberg.org/nimiux/coresubs
#  Run:
#   cpan Getopt::Long::Descriptive
#   cpan Math::Base::Convert
#   cpan Archive::Any
#   cpan Archive::Extract
#   cpan Email::Sender::Simple
#   cpan Email::Sender::Transport::SMTP::TLS
#   cpan Email::MIME
#   cpan Date::Calc
#   cpan IO:All
#   cpan Term::Readkey
#   cpan File::LibMagic
#   cpan Text::Template
#   cpan CAM::PDF
#   cpan File::Type
#   cpan LWP::UserAgent
#   cpan Net::SSH2
#   cpan MIME::Parser

#   cpan File:ShareDir::Install
#   cpan File::Slurp
#   cpan Algorithm::Permute
#   cpan Text::CSV
#   cpan List::AllUtils

PROJDIR=$(dirname $0)
MAKEFILE=Makefile.PL
[[ ! -d $PROJDIR ]] && echo "Invalid directory: $PROJDIR" && exit 1
[[ ! -f $PROJDIR/$MAKEFILE ]] && echo "No Makefile.PL found in directory $PROJDIR dir" && exit 2
cd $PROJDIR
perl Makefile.PL
make
#make test
#make manifest
#make dist
make install

