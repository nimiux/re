use strict;
use warnings;
use Re qw(
    $VERSION
);
use Test::More tests => 1;

use_ok("Re");

diag( "Testing Re $VERSION, Perl $], $^X" );

__END__

=encoding utf8

=head1 VERSION

This man page documents Re version 1.0.

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2025 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/re/issues

=cut
